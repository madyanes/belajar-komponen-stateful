import React from "react"
import { createRoot } from "react-dom/client"

class MyComponent extends React.Component {
    constructor(props) {
        // jika memanfaatkan konstruktor kelas, wajib memanggil base constructor (super) dan masukkan nilai props
        // Jika tidak, nilai this.props pada component akan bernilai undefined
        super(props)
        console.log('Component created!')
    }

    render() {
        return <p>Hello, {this.props.name}!</p>
    }
}

const root = createRoot(document.getElementById('root'))
root.render(
    <div>
        <MyComponent name="Dicoding" />
        <MyComponent name="Madyan" />
        <MyComponent name="Eka" />
    </div>
)
